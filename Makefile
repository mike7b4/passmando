release:
	@cd passmando && cargo deb
	@cd passmando && cargo rpm build
	@cp target/debian/*deb release
	@cp target/release/rpmbuild/RPMS/*/*rpm release

.PHONY: release
