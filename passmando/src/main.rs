use clap::Parser;
use cli_clipboard::{ClipboardContext, ClipboardProvider};
use libpassmando::errors::Error;
use libpassmando::passworddata::{make_password, make_random, PasswordData, Site};
use libpassmando::passwordinput::PasswordInput;
use libpassmando::passwordstore::PasswordStore;
use log::{error, info};

#[derive(Parser)]
struct ArgsAdd {
    #[clap(required = true)]
    site: String,
    #[clap(required = true)]
    login: String,
    /// Use text to make a hashed password.
    text: Option<String>,
}

#[derive(Parser)]
struct ArgsAddRaw {
    #[clap(required = true)]
    site: String,
    #[clap(required = true)]
    login: String,
    /// Store raw password entry instead of builtin hashed password generation.
    password: String,
}

#[derive(Parser)]
struct ArgsPrint {
    #[clap(required = true)]
    text: String,
    /// Store password in clipboard for 5 seconds
    #[clap(short, long)]
    clipboard: bool,
}

#[derive(Parser)]
struct ArgsSite {
    /// Only print password
    #[clap(short, long)]
    password_only: bool,
    /// Store password in clipboard for 5 seconds
    #[clap(short, long)]
    clipboard: bool,
    #[clap(required = true)]
    site: Site,
}

#[derive(Parser)]
struct ArgsRenameSite {
    /// Only print password
    #[clap(required = true)]
    site: Site,
    #[clap(required = true)]
    newname: Site,
}

#[derive(Parser)]
enum Action {
    /// Print a hashed password for given text.
    Password(ArgsPrint),
    /// Print all password information in the storage.
    ListAll,
    /// Print a random password hash, based on random words in passmando.
    Random,
    /// Print site password information.
    Site(ArgsSite),
    /// Add new site to storage.
    Add(ArgsAdd),
    /// Add new site to storage using a raw password instead of making a generated hashed password.
    /// You may use this when you must have special characters or when a site generates it.
    AddRaw(ArgsAddRaw),
    /// Remove site
    Remove(ArgsSite),
    Rename(ArgsRenameSite),
}

#[derive(Parser)]
#[clap(version, about)]
struct Args {
    /// Extra salt for a password.
    #[clap(short, long)]
    salt: Option<String>,
    /// Verbose(Debug) output
    #[clap(short, long)]
    verbose: bool,
    /// Print as json
    #[clap(short, long)]
    json: bool,
    #[clap(subcommand)]
    action: Action,
}

fn master_password() -> Result<String, Error> {
    let input = PasswordInput::new();
    let master = input.input()?;
    Ok(master)
}

fn action_add(gargs: &Args, args: &ArgsAdd) -> Result<(), Error> {
    let master = master_password()?;
    let mut pw = PasswordStore::load(&master)?;
    let empty_salt = String::new();
    let salt = &gargs.salt.clone().unwrap_or_else(|| "".into());
    let password = match &args.text {
        Some(p) => p.into(),
        None => {
            info!("Password not specified, passmando created random password.");
            let (_, password) = make_random(&salt)?;
            password
        }
    };

    pw.add(PasswordData::new(
        &args.site,
        &args.login,
        &password,
        &gargs.salt.clone().unwrap_or(empty_salt),
    ))?;
    pw.save(&master)?;
    println!("Saved password information:");
    pw.print(&args.site, false)?;
    Ok(())
}

fn action_add_raw(args: &ArgsAddRaw) -> Result<(), Error> {
    let master = master_password()?;
    let mut pw = PasswordStore::load(&master)?;
    let password = args.password.clone();
    pw.add(PasswordData::raw(&args.site, &args.login, &password))?;
    pw.save(&master)?;
    println!("Saved password information:");
    pw.print(&args.site, false)?;
    Ok(())
}

fn action_random(gargs: &Args) -> Result<(), Error> {
    let salt = &gargs.salt.clone().unwrap_or_else(|| "".into());
    let (s, p) = make_random(&salt)?;
    println!("{}", &s);
    println!("{}", &p);
    Ok(())
}

fn action_remove_site(gargs: &Args, args: &ArgsSite) -> Result<(), Error> {
    let master = master_password()?;
    let mut pw = PasswordStore::load(&master)?;
    let rpw = pw.remove(&args.site)?;
    pw.save(&master)?;
    if gargs.json {
        println!("{}", &serde_json::to_string(&rpw).unwrap());
    } else {
        info!("Removed password information");
        println!("{}", &rpw);
    }
    Ok(())
}

fn action_rename_site(gargs: &Args, args: &ArgsRenameSite) -> Result<(), Error> {
    let master = master_password()?;
    let mut pw = PasswordStore::load(&master)?;
    let rpw = pw.rename(&args.site, &args.newname)?;
    pw.save(&master)?;
    if gargs.json {
        println!("{}", &serde_json::to_string(&rpw).unwrap());
    } else {
        info!("Renamed: {} => {}", args.site, args.newname);
    }
    Ok(())
}

fn action_print_site(gargs: &Args, action_args: &ArgsSite) -> Result<(), Error> {
    let master = master_password()?;
    let pw = PasswordStore::load(&master)?;
    if gargs.json {
        pw.print_json(&action_args.site)?;
    } else if action_args.clipboard {
        let pw = pw.password(&action_args.site)?;
        let mut ctx = ClipboardContext::new().unwrap();
        info!("Password stored in clipboard");
        ctx.set_contents(pw).unwrap();
        std::thread::sleep(std::time::Duration::from_secs(5));
        ctx.set_contents("".into()).ok();
    } else {
        info!("Show password information");
        pw.print(&action_args.site, action_args.password_only)?;
    }
    Ok(())
}

fn action_list_all(gargs: &Args) -> Result<(), Error> {
    let master = master_password()?;
    let pwstore = PasswordStore::load(&master)?;
    if gargs.json {
        println!("{}", serde_json::to_string(&pwstore).unwrap());
    } else {
        println!("=== Show all passwords ===");
        println!("{}", pwstore);
    }
    Ok(())
}

fn action_password(gargs: &Args, pargs: &ArgsPrint) -> Result<(), Error> {
    let pw = make_password(
        &pargs.text,
        &gargs.salt.clone().unwrap_or_else(|| "".into()),
    );

    if pargs.clipboard {
        let mut ctx = ClipboardContext::new().unwrap();
        info!("Password stored in clipboard");
        ctx.set_contents(pw).unwrap();
        std::thread::sleep(std::time::Duration::from_secs(5));
        ctx.set_contents("".into()).ok();
    } else {
        println!("{}", pw);
    }

    Ok(())
}

fn run_main() -> Result<(), Error> {
    use env_logger::Builder;
    use log::LevelFilter;
    let args = Args::parse();
    if args.verbose {
        Builder::from_env("PASSMANDO_LOG")
            .filter(None, LevelFilter::Info)
            .init();
    } else {
        Builder::from_env("PASSMANDO_LOG").init();
    }

    match &args.action {
        Action::Password(pargs) => action_password(&args, &pargs),
        Action::Site(site_args) => action_print_site(&args, &site_args),
        Action::Random => action_random(&args),
        Action::Add(subargs) => action_add(&args, &subargs),
        Action::AddRaw(sargs) => action_add_raw(&sargs),
        Action::ListAll => action_list_all(&args),
        Action::Remove(sargs) => action_remove_site(&args, &sargs),
        Action::Rename(sargs) => action_rename_site(&args, &sargs),
    }
}

fn main() {
    if let Err(e) = run_main() {
        error!("{}", e);
        std::process::exit(i32::from(e));
    }
}
