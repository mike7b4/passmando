use crate::errors::Error;
use crate::passworddata::{PasswordData, Site};
use aead::{generic_array::GenericArray, Aead, NewAead};
use aes_gcm::Aes256Gcm;
use log::debug;
use ring::digest;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;
use std::fs::File;
use std::io::{Read, Write};
use std::path::PathBuf;
use typenum::U12;
#[derive(Serialize, Deserialize)]
pub struct PasswordStore {
    pub passwords: HashMap<Site, PasswordData>,
    #[serde(skip)]
    file_store: PathBuf,
}

impl Default for PasswordStore {
    fn default() -> Self {
        let mut file_store = dirs::config_dir().unwrap();
        file_store.push("passmando");
        std::fs::create_dir(&file_store).unwrap_or_else(|_| {});
        file_store.push("passmando.dat");
        Self {
            passwords: HashMap::new(),
            file_store,
        }
    }
}

impl fmt::Display for PasswordStore {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::new();
        for pw in self.passwords.values() {
            s += &format!("{}\n\n", pw);
        }
        write!(f, "{}", s)
    }
}

impl PasswordStore {
    fn aes256(password: &str) -> (GenericArray<u8, U12>, Aes256Gcm) {
        let p = digest::digest(&digest::SHA256, password.as_bytes());
        let nonce = GenericArray::clone_from_slice(
            &digest::digest(&digest::SHA256, b"passmando").as_ref()[0..12],
        );
        (
            nonce,
            Aes256Gcm::new(&GenericArray::clone_from_slice(&p.as_ref())),
        )
    }

    pub fn load(password: &str) -> Result<Self, Error> {
        let mut store = PasswordStore::default();
        if std::fs::metadata(&store.file_store).is_err() {
            return Ok(store);
        }
        let mut file = File::open(&store.file_store)?;
        let mut buf = vec![];
        file.read_to_end(&mut buf)?;
        let (nonce, cipher) = Self::aes256(password);
        let text = cipher
            .decrypt(&nonce, buf.as_ref())
            .map_err(|_| Error::DecryptionFailed)?;
        store = serde_json::from_slice(&text).map_err(|_| Error::DecryptionFailed)?;
        store.file_store = PasswordStore::default().file_store;
        Ok(store)
    }

    pub fn save(&self, password: &str) -> Result<(), Error> {
        debug!("Saved database: '{}'", &self.file_store.to_string_lossy());
        let input = serde_json::to_string(&self).unwrap();
        let (nonce, cipher) = Self::aes256(password);
        let ciphertext = cipher
            .encrypt(&nonce, input.as_bytes())
            .expect("encryption failure!");
        let mut file = File::create(&self.file_store)?;
        file.write_all(&ciphertext[0..])?;
        Ok(())
    }

    pub fn add(&mut self, pw: PasswordData) -> Result<(), Error> {
        self.passwords.insert(pw.site.clone(), pw);
        Ok(())
    }

    pub fn rename(&mut self, site: &str, newname: &str) -> Result<(), Error> {
        if self.passwords.get(newname).is_some() {
            return Err(Error::AlreadyExists(newname.into()));
        }
        // Remove old
        let mut old = self.remove(&site)?;
        old.site = newname.into();
        // Add new
        self.passwords.insert(newname.into(), old);
        Ok(())
    }

    pub fn remove(&mut self, site: &str) -> Result<PasswordData, Error> {
        self.passwords
            .remove(site)
            .ok_or_else(|| Error::SiteNotFound(site.into()))
    }

    pub fn password(&self, site: &str) -> Result<String, Error> {
        self.passwords
            .get(site)
            .map(|pw| pw.password.clone())
            .ok_or_else(|| Error::SiteNotFound(site.into()))
    }

    pub fn print(&self, site: &str, pw_only: bool) -> Result<(), Error> {
        let pw = self
            .passwords
            .get(site)
            .ok_or_else(|| Error::SiteNotFound(site.into()))?;

        if pw_only {
            println!("{}", pw.password);
        } else {
            println!("{}", pw);
        }

        Ok(())
    }

    pub fn print_json(&self, site: &str) -> Result<(), Error> {
        println!(
            "{}",
            &serde_json::to_string(
                &self
                    .passwords
                    .get(site)
                    .ok_or_else(|| Error::SiteNotFound(site.into()))?
            )
            .unwrap()
        );
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_pw_rename() {
        use crate::passwordstore::*;
        let mut pw = PasswordStore::default();
        assert_eq!(true, pw.add(PasswordData::new("foo", "", "", "")).is_ok());
        assert_eq!(true, pw.passwords.get("foo").is_some());

        assert_eq!(true, pw.rename("foo", "apa").is_ok());
        assert_eq!(false, pw.passwords.get("foo").is_some());
        let f = pw.passwords.get("apa").unwrap();
        assert_eq!(f.site, "apa");
    }

    #[test]
    fn test_pw_remove() {
        use crate::passwordstore::*;
        let mut pw = PasswordStore::default();
        assert_eq!(true, pw.add(PasswordData::new("epic", "", "", "")).is_ok());
        assert_eq!(true, pw.passwords.get("epic").is_some());

        assert_eq!(true, pw.remove("epic").is_ok());
        assert_eq!(false, pw.passwords.get("epic").is_some());
    }
}
