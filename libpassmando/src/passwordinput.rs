use crate::errors::Error;
use std::io::{self, prelude::*};
use termios::{tcsetattr, Termios, ECHO, TCSANOW};
/// Normally stdin echo characters. This module turn echo off when enter password
/// and reset it back to default when password has been entered.
pub struct PasswordInput {
    original: Termios,
}

impl PasswordInput {
    pub fn new() -> Self {
        let original = Termios::from_fd(libc::STDIN_FILENO).unwrap();
        let mut change = original;
        change.c_lflag &= !ECHO; // no echo and canonical mode
        tcsetattr(libc::STDIN_FILENO, TCSANOW, &change).unwrap();
        Self { original }
    }

    pub fn input(&self) -> Result<String, Error> {
        eprint!("Master password: ");
        let _ = std::io::stderr().flush();

        let input = io::stdin();
        let mut input = input.lock();
        let mut buf = String::new();
        input
            .read_line(&mut buf)
            .map_err(|_| Error::MissingMasterPassword)?;
        println!();
        Ok(buf.replace('\n', ""))
    }
}

/// Drop trait implemented to make sure terminal is reset to default
/// when PasswordInput goes out of scope.
impl Drop for PasswordInput {
    fn drop(&mut self) {
        tcsetattr(libc::STDIN_FILENO, TCSANOW, &self.original).unwrap();
    }
}
