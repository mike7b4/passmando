use std::fmt;
pub enum Error {
    MissingMasterPassword,
    MissingPassword,
    DecryptionFailed,
    AlreadyExists(String),
    SiteNotFound(String),
    IO(std::io::Error),
    BindAddress(std::io::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Error::*;
        match &self {
            MissingMasterPassword => write!(f, "The selected action needs a master password!"),
            MissingPassword => write!(f, "The selected action needs a password!"),
            DecryptionFailed => write!(f, "Decryption failed, Did you pass wrong master password?"),
            AlreadyExists(site) => write!(f, "Site '{}' already exists", site),
            SiteNotFound(site) => write!(f, "Site '{}' is not found in database.", &site),
            IO(e) => write!(f, "{}", e),
            BindAddress(e) => write!(f, "Bind address failed with {}", e),
        }
    }
}

impl From<Error> for i32 {
    fn from(e: Error) -> i32 {
        use Error::*;
        match &e {
            MissingMasterPassword => 64,
            MissingPassword => 65,
            DecryptionFailed => 66,
            AlreadyExists(_) => 67,
            SiteNotFound(_) => 68,
            IO(_) => 80,
            BindAddress(_) => 81,
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(io: std::io::Error) -> Self {
        Error::IO(io)
    }
}
