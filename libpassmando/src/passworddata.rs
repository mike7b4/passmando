use crate::errors::Error;
use crate::words::random_word;
use base64::encode;
use ring::digest;
use serde::{Deserialize, Serialize};
use std::fmt;

pub type Site = String;
#[derive(Serialize, Deserialize)]
pub struct PasswordData {
    pub site: Site,
    pub login: String,
    pub password: String,
}

impl fmt::Display for PasswordData {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            r###"Site: {}
Login: {}
Password: {}"###,
            &self.site, &self.login, &self.password
        )
    }
}

impl PasswordData {
    pub fn new(site: &str, login: &str, password: &str, salt: &str) -> Self {
        let password = &make_password(password, salt);
        PasswordData::raw(site, login, password)
    }

    pub fn raw(site: &str, login: &str, password: &str) -> Self {
        Self {
            site: site.into(),
            login: login.into(),
            password: password.into(),
        }
    }
}

/// Creates a SHA256 hashed password from string input and encode it as base64[0..16]
pub fn make_password(password: &str, salt: &str) -> String {
    let input = &format!("{}{}", password, salt);
    let p = digest::digest(&digest::SHA256, input.as_bytes());
    let mut hexify = String::new();
    for b in p.as_ref() {
        hexify += &format!("{:02x}", b);
    }
    encode(&hexify)[0..16].to_string()
}

/// Creates an SHA256 hashed password from random words and return it as base64[0..16]
pub fn make_random(salt: &str) -> Result<(String, String), Error> {
    let mut s = String::new();
    for _ in 0..11 {
        s += &format!("{} ", &random_word());
    }
    s += &random_word();
    Ok((s.clone(), make_password(&s, &salt)))
}
