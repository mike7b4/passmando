use libpassmando::passwordstore::PasswordStore;
use rocket::http::{Cookie, Cookies};
use rocket::outcome::IntoOutcome;
use rocket::request::{self, FlashMessage, Form, FromRequest, Request};
use rocket::response::{Flash, Redirect};
use rocket::*;
use rocket_contrib::{
    serve::{crate_relative, StaticFiles},
    templates::Template,
};
use std::collections::HashMap;

#[derive(FromForm)]
struct Login {
    //    username: String,
    password: String,
}
struct User(String);

#[rocket::async_trait]
impl<'a, 'r> FromRequest<'a, 'r> for User {
    type Error = std::convert::Infallible;

    async fn from_request(request: &'a Request<'r>) -> request::Outcome<User, Self::Error> {
        request
            .cookies()
            .get_private("pw")
            .and_then(|cookie| cookie.value().parse().ok())
            .map(|id| User(id))
            .or_forward(())
    }
}

#[post("/login", data = "<login>")]
fn login(mut cookies: Cookies<'_>, login: Form<Login>) -> Result<Redirect, Flash<Redirect>> {
    cookies.add_private(Cookie::new("pw", login.password.clone()));
    Ok(Redirect::to(uri!(index)))
}

#[post("/logout")]
fn logout(mut cookies: Cookies<'_>) -> Flash<Redirect> {
    cookies.remove_private(Cookie::named("user_id"));
    Flash::success(Redirect::to(uri!(login_page)), "Successfully logged out.")
}

#[get("/login")]
fn login_user(_user: User) -> Redirect {
    Redirect::to(uri!(index))
}

#[get("/login", rank = 2)]
fn login_page(flash: Option<FlashMessage<'_, '_>>) -> Template {
    let mut context = HashMap::new();
    if let Some(ref msg) = flash {
        context.insert("flash", msg.msg());
        if msg.name() == "error" {
            context.insert("flash_type", "Error");
        }
    }

    Template::render("login", &context)
}

#[get("/")]
fn user_index(user: User) -> Template {
    let mut ctx = HashMap::new();
    if let Ok(passwords) = PasswordStore::load(&user.0) {
        ctx.insert("passwords", passwords.passwords);
        Template::render("index", &ctx)
    } else {
        log::error!("password failed");
        Template::render("index", &ctx)
    }
}

#[get("/", rank = 2)]
fn index() -> Redirect {
    Redirect::to(uri!(login_page))
}

#[launch]
fn rocket() -> Rocket {
    ignite()
        .attach(Template::fairing())
        .mount(
            "/",
            routes![index, user_index, login_user, login_page, login],
        )
        .mount("/css/", StaticFiles::from(crate_relative!("/css")))
}
