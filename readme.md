# Passmando

A simple password manager.

If you are sick and tired of select a password you can create random password based on a text input.

Password are stored AES256 encrypted in directory ~/.config/passmando/passmando.dat using a master password.

# How to

## Add/Change site password with a randomized password

```passmando add protonmail.com some_email@protonmail.com```

## Add/Change new password with a hashed password created from text

```passmando add google.com myemail@google.com "hello world"```

## Remove password entry.

```passmando remove facebook.com```

## Print an encrypted password created from text input

```passmando password "hello world"```

## Print an random created password

```passmando random```

## Print a stored password for a site

```passmando site SITE```

## Print all stored passwords

```passmando print-all```

## Print all stored passwords as json

```passmando --json list-all```

## How to pipe password to clipboard and make it stay for 15 seconds

Add this to your .bashrc and make sure xclip is installed.

```
pw-site() {
	local p
	if p=$(passmando site "${1:?Expect a site}"); then
		echo "$p" | xclip -i -sel clip
		reset
		history -d -1
		(sleep 15 && echo -n "" | xclip -i -sel clip)
	fi
}
```
